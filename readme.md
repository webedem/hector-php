Introduction
============

Hector is a light PHP template framework which seperates the HTML syntax from the PHP code. Hector also allows you to translate
a website on-the-fly without the need of editing any original files.

**Latest Version 1.08**



Understanding the file structure
================================
The file structure consists of the main class file (*hector.class.php*) and three folders (*/_pages/*, */_server/* and */langs/*). Instead of having HTML code within a PHP file, Hector seperates the content into 2 files:
* an HTML file containing only HTML tags and Hector variables (moustache like)
* a PHP file which connects the server side logic with the respective HTML file

_So instead of having a sample.php file with embeded HTML tags, you now have a sample.html (which includes only HTML tags) under the /_pages/ folder, and a sample.php file under /_server/ folder (which handles the automatic embedding and rendering of the HTML code). We state more about the /langs/ folder and contents later on._

Within the HTML files, you can have a set of variables, which will be handled by the respective PHP file and the Hector framework. These variables are denoted with { } (similar to moustache). Apart from the variables, Hector handles logical statements like if/else and loop/end loop. You may read all about the Hector syntax later on on the documentation.


Configuration
=============
* $verbose (default: true) : if you want to immediately output the rendered HTML or save it into a PHP variable
* $path (default: ../) : The relative path to hector.class.php


How to use
==========

An empty PHP template file will require the following:

```
<?php 
	require('hector.class.php'); 
	$hector = new Hector(); 
	$hector->render(); 
?>
```


Documentation
=============

Basic example
-------------


To set a variable within the HTML page you simply need to type  {VARIABLE_ID} where VARIABLE_ID is the name of this Hector variable. Variable id can be anything you want, but it needs to be unique within a single HTML file. 

Example: 
`<p>{TITLE_HERE}</p>`

To render the Hector variable, you need to handle it in your php file. To render a Hector variable in PHP you need to do the following:

Example: 
```
<?php 
	require('hector.class.php'); 
	$hector = new Hector(); 
	$hector->set("TITLE_HERE", "This will be the title of the webpage", "txt"); 
	$hector->render(); 
?>
```

The SET function will find the {TITLE_HERE} occurance in the HTML file and will replace it with the provided string. We indicate that the type of variable is a string (integers are handled as strings) with the type txt (or text)

Hector Syntaxing
----------------

All syntaxing will be handled by the SET function in Hector. The SET function receives 3 mandatory variables and 1 optional. Let's see the SET function:

`public function set($key, $val, $type="text", $class=""){`

* **KEY**: the HTML variable name/id
* **VAL**: the value which will replace the HTML variable
* **TYPE**: the type of replacement (more details will follow)
* **CLASS** (optional): the CSS classname for the specific element

Valid types are:

* **txt, text**: Renders string, numeric values (words, phrases, paragraphs)
* **li, list**: Adds list elements inside a <ul> element
* **option**: Adds option elements within a <select> element
* **th, table header**: Adds the table headers within a <table> element
* **td, table rows**: Adds table rows within a <table> element
* **html**: Embed an html file within the current HTML file (Ahh! inception ;))
* **loop/end**: Render any HTML element found between this syntax in a loop
* **if/else/endif**: Render only the HTML elements associated with a logical function
* **array**: Convert all occurances of a variable denoted with brackets within a loop to a specified string (v. 1.05+)

*Detailed syntaxing can be found in /_pages/sample.html and /_server/sample.php*


Translation
-----------

Hector reads an HTML file, renders it and outputs the compiled and rendered result to your browser. With Hector it is very easy to translate the result output in another language, without needing to access the original .php or .html files. If you need to translate in another language you need to have a language file under the /lang/ folder (i.e el.data.txt - el is for Greek).

The language file is a multiline text file with strings in the original language and their translation, seperated by |. Hence, if you want to translate any string, you only need to include it in your translation file and tell Hector you want to translate the output to a different language.

Example:
`$hector->setTranslation('el');`


Demo & More Examples
--------------------

Checkout the sample.php and sample.html file for examples on all above syntaxes


Change Log
----------

Version 1.08:
+Added support for file inclusions (main page controller)

Version 1.07:

+Performance fixes

Version 1.06:

+Minor fixes

Version 1.05:

+Added the ARRAY type. Will work within a current loop to replace more items

Version 1.04: 

+Added the PATH variable








