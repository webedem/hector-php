<?php
/**
 * Copyright 2014 WebEdem Web & Media Services Ltd. 
 * Written by Kris Leonidou
 * Latest release: https://bitbucket.org/webedem/hector-php/
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
 
 
# HECTOR is similar to SMARTY 
	
class Hector{
	
	public $verbose = "true";
	public $path = "";

	private $vars = array();		
	private $page_file = "";
	private $mtime = "";
	private $start = "";		
	private $translation_lang = "";	
	private $debug = false;			
	
	private static $version = "1.08";
	
	// CONSTRUCTOR
	function __construct($script_file = "") {
		
		if($this->debug==true){
			  $this->mtime = microtime();
			  $this->mtime = explode(' ', $this->mtime);
			  $this->mtime = $this->mtime[1] + $this->mtime[0];
			  $this->start = $this->mtime;
		}							
		
		// script_file variable is only used if you have a controller page to handle requests and page inclusion based on a GET/POST variable (i.e index.php?page_id=PAGE)
		$this->setFilePath($script_file);				
	}
	
	
	// SET THE FILE PATH
	public function setFilePath($script_file){
		if(!isset($script_file) || empty($script_file)){
			$tmpfilename = explode("/", $_SERVER['SCRIPT_NAME']);
			$this->page_file = str_replace(".php", "", end($tmpfilename));
			unset($tmpfilename);
		}
		else{
			$this->page_file = str_replace(".php", "", $script_file);			
		}
	}
	

	// Append values to the values array
	public function set($key, $val, $type="text", $class=""){
		if(!isset($key) || empty($key)){
			$this->showLog("Key missing or undefined");
		}
		else{					
			if(!isset($val)){
				$this->showLog("Value for element ".$key." is undefined");
			}
			else{
				array_push($this->vars, array("id"=>$key, "type"=>$type, "class"=>$class, "values"=>array($val)));							
			}			
		}
	}

	public function render(){	
		if(!file_exists($this->path.'_pages/'.$this->page_file.'.html')){
			die("Page file ".$this->page_file.".html cannot be found.");
		}
		else{
		
			$output =  file_get_contents($this->path.'_pages/'.$this->page_file.'.html');	
			
			
			
			foreach($this->vars as $key=>$value){
				switch($value['type']){
					case 'option':
						$output_items = '';
						foreach($value['values'][0] as $item){
						$output_items .= '<option value="'.array_search($item, $value['values'][0]).'"'.(empty($value['class']) ? '' : ' class="'.$value['class'].'"').'>'.$item.'</option>';							
						}
						$output = str_replace("{".$value['id']."}", $output_items, $output);	
						break;
					case 'table header';
					case 'th':
						$output_items = '';
						foreach($value['values'][0] as $item){
							$output_items .= '<th'.(empty($value['class']) ? '' : ' class="'.$value['class'].'"').'>'.$item.'</th>';
						}
						$output = str_replace("{".$value['id']."}", $output_items, $output);	
						break;		
					case 'table rows';
					case 'td':
						$output_items = '';
						foreach($value['values'][0] as $item){

							if(count($item)==1){
								$output_items .= '<td'.(empty($value['class']) ? '' : ' class="'.$value['class'].'"').'>'.$item.'</td>';
							}
							else{
								$output_items .= '<tr>';
								foreach($item as $table_row){
									$output_items .= '<td'.(empty($value['class']) ? '' : ' class="'.$value['class'].'"').'>'.$table_row.'</td>';	
								}								
								$output_items .= '</tr>';
							}
						}
						$output = str_replace("{".$value['id']."}", $output_items, $output);	
						break;										
					case 'li';
					case 'list':
						$output_items = '';
						foreach($value['values'][0] as $item){
							$output_items .= '<li'.(empty($value['class']) ? '' : ' class="'.$value['class'].'"').'>'.$item.'</li>';
						}
						$output = str_replace("{".$value['id']."}", $output_items, $output);	
						break;	
					case 'html':
						preg_match_all("/\[html{(.*?)}\]/si", $output, $matches);	
						foreach($matches[1] as $item){
							if(stristr($item, $value['id'])==true){									
								$output = str_replace("[html{".$value['id']."}]", file_get_contents('_pages/'.$value['values'][0].'.html'), $output);
							}
							
						}												
						break;	
					case 'array':
					
						$output_items = '';	
						preg_match_all("/{(".$value['id'].")\[]}/si", $output, $match);	
						
						if(count($match[1])>=1){																											
							for($i = 0; $i<count($match[1]); $i++){
								$output = preg_replace("/{(".$value['id'].")\[]}/si", $value['values'][0][$i], $output, 1);
							}
						}
	
						break;													
					case 'loop':						
						$output_items = '';
	
						preg_match("/%loop%(.*?)\\%end%/si", $output, $match);				
						
						if(stristr($match[1], $value['id'])==true){														
							foreach($value['values'][0] as $item){	
								$output_items .= str_replace("{".$value['id']."}", $item, $match[1]);
							}
							 
							$output = preg_replace("/%loop%(.*?)".$value['id']."(.*?)\\%end%/si", $output_items, $output);
						}						
						
						break;	
					case 'if':	
						$output_items = '';
						preg_match("/%if(.*?)%/si", $output, $match);														
						if(stristr($match[1], $value['id'])==true){														
							$output = preg_replace("/%if".$match[1]."%(.*?)\\%else%(.*?)%endif%/si", "$1", $output);					
							$output = str_replace("{".$value['id']."}", $value['values'][0], $output);	
						}
						
						break;	
					case 'else':	
						$output_items = '';

						preg_match("/%if(.*?)%/si", $output, $match);														
						if(stristr($match[1], $value['id'])==true){														
							$output = preg_replace("/%if".$match[1]."%(.*?)\\%else%(.*?)%endif%/si", "$2", $output);					
							$output = str_replace("{".$value['id']."}", $value['values'][0], $output);	
						}
						
						break;							
					case 'text';	
					default:
						$output = str_replace("{".$value['id']."}", $value['values'][0], $output);	
				}								
			}
			
			
			// Check if the translation feature is enabled and whether a translation language is set
			if($this->translation_lang!=""){
				$output = $this->translate($output, $this->translation_lang);	
			}
					
			// Is verbose mode on?		
			if($this->verbose==true){ echo $output; } else { return $output; }
			
			// Is debug mode on?	
			if($this->debug==true){
				$this->mtime = microtime();
				$this->mtime = explode(' ', $this->mtime);
				$this->mtime = $this->mtime[1] + $this->mtime[0];
				$finish = $this->mtime;
				$total_time = round(($finish - $this->start), 4);
				echo '<div class="alert alert-warning">
                        <b>DEBUG:</b> Page generated in '.$total_time.' seconds.
                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>';
			}
			
		}
	}
	
	public function setTranslation($lang){
		if(!isset($lang) || empty($lang)){
			
		}
		else{
			$this->translation_lang = $lang;			
		}
	}
	
	private function mb_stripos_all($haystack, $needle) {
			  $s = 0;
			  $i = 0;
			 
			  while(is_integer($i)) {
			 
				$i = mb_stripos($haystack, $needle, $s);
			 
				if(is_integer($i)) {
				  $aStrPos[] = $i;
				  $s = $i + mb_strlen($needle);
				}
			  }
			 
			  if(isset($aStrPos)) {
				return $aStrPos;
			  } else {
				return false;
			  }
	}
	
	private function translate($stream, $lang='en'){
		
		$output = $stream;
		
		
		if(file_exists($this->path.'langs/'.$lang.'.data.txt')){
			$file_stream = fopen($this->path.'langs/'.$lang.'.data.txt', 'r');
			while(!feof($file_stream)) {				
			   $translation = explode("|", fgets($file_stream));	
			   if(count($translation)>1){	
				    $output = preg_replace("~<a[^>]*>.*?</a\s*>(*SKIP)(*FAIL)|\b".$translation[0]."\b~s", rtrim($translation[1]), $output);					
			   }
			}
			
			fclose($file_stream);
		}

		
		return $output;
	}
	
	private function showLog($msg){
			if($this->debug==true){
				ini_set("display_errors", "1");
				error_reporting(E_ALL);
				exit($msg);        
			}                        
	  }


}

?>
